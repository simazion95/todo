<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'moria',
                    'email' => 'moria@moria.com',
                    'password' =>Hash::make('12345678') ,
                    'role'=>'emloyee',
                    'created_at'=>date('Y-m-d G:i:s'),
                    'updated_at'=>date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'lior',
                    'email' => 'lior@lior.com',
                    'password' =>Hash::make('12345678'),
                    'role'=>'emloyee',
                    'created_at'=>date('Y-m-d G:i:s'),
                    'updated_at'=>date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'la',
                    'email' => 'la@la.com',
                    'password' =>Hash::make('12345678'),
                    'role'=>'emloyee',
                    'created_at'=>date('Y-m-d G:i:s'),
                    'updated_at'=>date('Y-m-d G:i:s'),
                ],
        
            ]); 
    }
}
