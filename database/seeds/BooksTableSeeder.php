<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
                    'title' => 'Harry Potter 1',
                    'author' => 'J.K. Rowling',
                ],
                [
                    'title' => 'Harry Potter 2',
                    'author' => 'J.K. Rowling',
                ],
                [
                    'title' => 'Harry Potter 3',              
                    'author' => 'J.K. Rowling',
                ],
                [
                    'title' => 'Harry Potter 4',                 
                    'author' => 'J.K. Rowling',
                ],      [
                    'title' => 'Harry Potter 5',                  
                    'author' => 'J.K. Rowling',
                ],
            ]);
    }
}
