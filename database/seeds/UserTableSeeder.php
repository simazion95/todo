<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'jack',
                    'email' => 'jack@jack.com',
                    'password' => '12345678',
             
                ],
                [
                    'name' => 'john',
                    'email' => 'john@john.com',
                    'password' => '12345678',
                   
                ],
            
            ]); 
    }
}
