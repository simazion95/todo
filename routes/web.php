<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|------------------------------------------
-------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return "HELLO WORLD";
});

Route::get('/student/{id?}', function ($id=' no student provided') {
    return "HELLO Student".$id ;
})->name('student');

Route::get('/comment/{id}', function ($id) {
    return view('comment',['id'=>$id]);
})->name('comments');

/*route its class ,resource - statti */
Route::resource('todos','TodoController')->middleware('auth');


Route::resource('books','BookController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
